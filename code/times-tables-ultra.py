# Initalize variables.
is_error = False
skipped_questions_num = 0
correct_answers = 0
times_table = 0
choice = 1
has_displayed_title = False
incorrect_answers = 0
mode = None

def provide_summary():
    # Provide a summary.
    print("\nYou completed this times table!")
    print("You got ", correct_answers," questions correct.")
    print("You skipped ", skipped_questions_num, " questions.")
    print("You got", incorrect_answers, " wrong.")    

def practice_mode(): 
    while choice == 1:
        for index in range(1,13):
            print(index, " x ", times_table)
            answer = input("")
            answer.strip()
            if answer == "skip":
                print("Skipped")
                skipped_questions_num += 1
                continue
            else:
                answer_int = int(answer)
                print("You answered", answer_int)

                if answer_int == int(index*times_table):
                    print("Correct answer! +1 point.")
                    correct_answers += 1
                else:
                    print("Incorrect answer.")
                    incorrect_answers += 1
        
while True: 
    if has_displayed_title == False:
        print("\nWelcome to Times Tables Ultra!")
        has_displayed_title = True
    # TODO: Add different modes.
    try:
        print("Mode 1: Practice")
        mode = int(input("Which mode would you like to try?"))
    except:
        is_error = True
        print("Please type 1.")
        while is_error == True:
            print("Mode 1: Practice")
            mode = int(input("Which mode would you like to try?"))
    else:
        print("You are using mode ", mode,".\n")

    try:
        # Ask which times table.
        times_table = int(input("Which times table would you like to practice?"))
    except:
        # If user entered a non-integer then the loop runs until they do.
        is_error = True
        print("\nPlease enter a integer.")
        while is_error == True:
            try:
                times_table = int(input("Which times table would you like to practice?"))
            except:
                print("\nPlease enter a integer.")
                is_error = True
            else:
                # Once they have typed an integer.
                is_error = False
                print("You are practicing the ", times_table, " times table.")
    else:
        # If they entered a integer first.
        print("You are practicing the ", times_table, " times table.\n")
        
        if mode == 1:
            practice_mode() # TODO: Add different modes.
        elif mode == 2:
            pass
        else:
            pass
    
        provide_summary()
        
        try:    
            choice = int(input("Type 1 to retry this times table and type 2 to chose a different one."))  
        except:
            # If user entered a non-integer then the loop runs until they do.
            is_error = True
            print("\nPlease enter 1 or 2.")
            while is_error == True:
                try:
                    choice = int(input("Which choice?"))
                except:
                    print("\nPlease enter 1 or 2.")
                    is_error = True
                else:
                    if choice != 1 or 2:
                        is_error = True
                        print("\nPlease enter 1 or 2.")
                        continue
                    # Once they have typed an integer.
                    is_error = False
                    print("You chose option ", choice, "\n")
        else:
            # If they entered a integer first.
            print("You chose option ", choice, "\n")    

