# Times Tables Ultra

Welcome to Times Tables Ultra!

Times Tables Ultra hopes to provide an excellent learning experience for all.
This repository was created to find new developers and to spread the word about Times Tables Ultra.

## Contribution
TTU is actively developed by @Cup_Of_Java but no other developers have applied yet, so be the first to do so!

If you would like to contribute then please follow these steps:

1. Request access to the repository.
2. Create an issue named 'Developer Request'.

Your application will be accepted as soon as possible by one of the moderators.

## Roadmap
- [ ] We plan to have an intital release in September which will include a practice mode and a timed mode.
- [ ] We will then have a second version released that will include targeted questions, settings, a review mode and much more.

- [ ] The final release will then include many exciting new features that will be announced soon.

## Members
### Moderators:
@Cup_Of_Java
### Developers:
None yet, read the Contribution section to find out how to become one.
## FAQ
### What is Times Tables Ultra?
- Times Tables Ultra is a times tables practice program inspired by Times Tables Rockstars.
### What language is it made in?
- Times Tables Ultra is made in Python mainly using the Tkinter library for UI aspects.
### How can I contribute?
- Please read the Contribution section.
